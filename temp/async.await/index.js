const request = require('request');


// // async function getUserName(userld) {
// //   var user = await getUser(userid);
// //   return user.name;
// // }


let pc = {};

async function getRequest(url) {

  let promise = new Promise((resolve, reject) => {

    request(url, function(error, response, body) {
        console.log("1: ");
        if (!error && response.statusCode == 200)
          resolve(body);
        else
          reject("error");
    });

  });


  let body = await promise;

  console.log("2: ");
  return body;
}

(async function() {
  let pc = await getRequest('https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json');
  console.log("3: "+pc);
})()
