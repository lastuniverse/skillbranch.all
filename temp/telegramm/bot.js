// messanger
// Good. Now let's choose a username for your bot. It must end in `bot`. Like this, for example: TetrisBot or tetris_bot.

// lastuniverseBot
// Done! Congratulations on your new bot. You will find it at telegram.me/lastuniverseBot. You can now add a description, about section and profile picture for your bot, see /help for a list of commands. By the way, when you've finished creating your cool bot, ping our Bot Support if you want a better username for it. Just make sure the bot is fully operational before you do this.

// Use this token to access the HTTP API:
// 286198519:AAHK1xC-uVsxkf9gQSkZ4aDyPUwswqKu5wE

// For a description of the Bot API, see this page: https://core.telegram.org/bots/api

/*
var TelegramBot = require('node-telegram-bot-api');
var token = '286198519:AAHK1xC-uVsxkf9gQSkZ4aDyPUwswqKu5wE';
var bot = new TelegramBot(token, {polling: true});

bot.on('message', function (msg) {
    var chatId = msg.chat.id;
    console.log(msg);
    bot.sendMessage(chatId, 'Hello!', {caption: 'I'm a roBot!'});
});
*/


const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');
const config = require('./data/config/config.json');


var bot = new TelegramBot(config.token, config.options);

// bot.sendMessage('@SkillBranch_NodeJS', '*Всем привет*. Я маленький бот-ик Петра Великого :) `/help` мне в ...', {
//   caption: 'I`m a roBot!',
//   parse_mode: 'Markdown'
// });


bot.onText(/.*/, function (msg) {
  console.log("onTEXT: "+JSON.stringify(msg));
});


bot.on('text', function(msg) {
  console.log("TEXT: "+JSON.stringify(msg));
  if(msg.from.id == 279262834){
    bot.sendMessage('@SkillBranch_NodeJS', msg.text , {});
  }
});


bot.on('message', function(msg) {
  console.log("MSG: "+JSON.stringify(msg));
  // if (msg.text) {
  //   const message = msg.text.split(/\s+/);
  //   if (message.length) {
  //     const command = message.shift();
  //     if (command in commands) {
  //       commands[command].callback(msg, message);
  //     }
  //   }
  // }
});


let commands = {
  '/help': {
    'info': '*/help* - выводит список всех доступных команд\n*/help commandname* - выводит описание команды commandname'
  },
  '/join': {
    'info': '*/join @chatname* - входит в чат chatname'
  },
  '/photo': {
    'info': '*/photo* - покажу мою вотографию :)'
  }
}

commands['/help'].callback = function(msg, params) {
  var chatId = msg.chat.id;
  // if (params.length) {
  //   if (params[0] in commands)
  //     bot.sendMessage(chatId, commands[params[0]].info, {
  //       caption: 'I`m a roBot!',
  //       parse_mode: 'Markdown'
  //     });
  //   else
  //     bot.sendMessage(chatId, 'Я не знаю такой команды - `' + params[0] + '`.\nПишите `/help` для получения списка команд', {
  //       caption: 'I`m a roBot!',
  //       parse_mode: 'Markdown'
  //     });
  // } else {
  //   let answer = 'Команды, которые я понимаю:';
  //   for (let key in commands) {
  //     answer += '\n' + commands[key].info;
  //   }
  //   bot.sendMessage(chatId, answer, {
  //     caption: 'I`m a roBot!',
  //     parse_mode: 'Markdown'
  //   });
  // }
}

commands['/photo'].callback = function(msg, params) {
  var chatId = msg.chat.id;
  // fs.exists(config.photo, function(exists) {
  //   if (exists)
  //     bot.sendPhoto(chatId, config.photo, {
  //       caption: 'It`s my photo! I`m a roBot'
  //     });
  // });
}

commands['/join'].callback = function(msg, params) {
  var chatId = msg.chat.id;
  // bot.sendMessage(chatId, 'Сорян браток, ничего личного, но эта команда не для тебя :)', {
  //   caption: 'I`m a roBot!'
  // });
}
