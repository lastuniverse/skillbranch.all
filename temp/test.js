app.get(/task3a/, (req, res) => {
  let result = pc;
  let status = 200;
  let volume = {
    "C:": 0,
    "D:": 0
  };
  // Получаем URL, розбиваем
  let query = req.originalUrl.split('/');
  // Убираем /task3a
  query.splice(0, 2);
  // Если запрос не /task3a || /task3a/
  if (!(query.length === 0 || query[0] === "")) {
    // Если запрос объема жестких дисков
    if (query[0] === "volumes") {
      // Подсчет объема HDD
      for (let i = 0; i <= pc.hdd.length - 1; i++) {
        volume[pc.hdd[i]['volume']] += pc.hdd[i]['size'];
      }
      volume['C:'] = volume['C:'] + 'B';
      volume['D:'] = volume['D:'] + 'B';
      result = volume;
    } else {
      // Осталвные запросы
      for (let i = 0; i <= query.length - 1; i++) {
        // Определяем конец
        if (query[i] === "") {
          break;
        }
        // Если ошибочное пале
        if (result[query[i]] === undefined && query[i] !== "volumes") {
          status = 404;
          result = "Not Found";
          break;
        }
        // Если ошибка в ram/volume / volumes
        if (query[i] === "volumes") {
          result = result["volume"];
          break;
        }
        // Обновляем результат если все /орано
        result = result[query[i]];
      }
    }
  }

  console.log(status);
  res.status(status).json(result);
});
