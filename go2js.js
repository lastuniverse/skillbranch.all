var go2js = require("node-go2js");

//Wrap erlang code with:
//"-module(tmp).\n-compile({parse_transform, shen}).\n-compile(export_all).\n-js([start/0]).\nstart() ->\n";
go2js.wrap(code, function(ast) {
      //ast: the js obj holding the AST tree
});

//You can also set optional import standard lib names to avoid compile-time errors
go2js.wrap(code, ["fmt", "math"], function(ast) {
      //ast: the js obj holding the AST tree
});

//Parse complete erlang code
go2js.parse(code, function(ast) {
      //ast: the js obj holding the AST tree
});
