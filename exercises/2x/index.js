var express = require('express');
var cors = require('cors');


const app = express();
app.use(cors());

app.get('/', function (req, res) {
  //console.log(req.query);
  let result = 1;
  if(req.query.i){
    const i = req.query.i;
    result = lagrangeCalc(i);
    console.log("["+i+"]"+result);
    // if( i==1 ){
    //   result = 18;
    // }
  }
  res.send(""+result);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});




function lagrangeCalc(x){
  // для расчетов используется полином лагранджа.
  // но так как он линейный, то для его точной работы требуются значения всех контрольных точек :)
  // хотелось бы увидеть эту сумашежшую фанку в реальности f(x)=13**x+....

  const arrX = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
  const arrY = [1, 18, 243, 3240, 43254, 577368, 7706988, 102876480, 1373243544, 18330699168, 244686773808,3266193870720,43598688377184,581975750199168,7768485393179328,103697388221736960,1384201395738071424,18476969736848122368,246639261965462754048];
  let result = 0;
  for(let i=0;i<arrX.length;i++){
    const subres = lagrangeCalcSub(x,i,arrX);
    result = result + (arrY[i]*subres);
  }
  return result;
}


function lagrangeCalcSub(x,i,arr){
  let result = 1;
  for(let j=0;j<arr.length;j++){
    if(j != i){
      result = result * (x-j)/(i-j);
    }
  }
  return result;
}
