'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var fetch = require('fetch');


var app = express();
app.use(cors());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.get('/volumes', function(req, res) {
    let v = {};
    for (let key in pc.hdd) {
        const d = pc.hdd[key].volume;
        if (!v[d])
            v[d] = 0;
        v[d] += pc.hdd[key].size;
    }
    for (let key in v) {
        v[key] = v[key] + 'B';
    }
    console.log(v);
    res.json(v);
});

app.get(/^(?:\/)?(.*?)(?:\/)?$/, function(req, res) {
    const result = getValue(pc, req.params[0]);
    if (result == 'Not Found')
        res.status(404).send('Not Found');
    else
        res.json(result);
});

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
let pc = {};

fetch.fetchUrl(pcUrl, function(err, meta, body) {
    pc = JSON.parse(body);
    console.log('Все загружено. Можно пускать тесты');

});

function getValue(json, path) {
    let result = 'Not Found';

    if (!path)
        return json;

    if (typeof path === 'string') {
        console.log("path: [" + path + "]");
        if (/[^\/\w]/.test(path))
            return result;
        path = path.split(/\//);
    }

    const key = path.shift();

    if (!json.hasOwnProperty(key))
        return result;

    if (json.constructor()[key] !== undefined)
        return result;

    if (path.length) {
        result = getValue(json[key], path);
    } else {
        result = json[key];
    }

    return result;
}
