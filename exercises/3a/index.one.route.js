'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var fetch = require('fetch');


var app = express();
app.use(cors());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
	extended: true
})); // for parsing application/x-www-form-urlencoded

let pc = {};

app.get(/^(?:\/)?(.*?)(?:\/)?$/, function(req, res) {
	let result = 'stope :)';
	let path = req.params[0];

	if (path == 'volumes')
		result = getVolumes(pc.hdd);
	else
		result = getValue(pc, path);

	if (result == 'Not Found')
		res.status(404).send(result);
	else
		res.json(result);
});

app.listen(3000, function() {
	console.log('Example app listening on port 3000!');
});


const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
fetch.fetchUrl(pcUrl, function(err, meta, body) {
	pc = JSON.parse(body);
	console.log('Все загружено. Можно пускать тесты');

});

function getVolumes(hdd) {
	let v = {};
	for (let key in hdd) {
		const d = hdd[key].volume;
		if (!v[d])
			v[d] = 0;
		v[d] += hdd[key].size;
	}
	for (let key in v) {
		v[key] = v[key] + 'B';
	}
	return v;
}

function getValue(json, path) {
	let result = 'Not Found';

	if (!path)
		return json;

	console.log("path: [" + path + "]");

	if (/[^\/\w]/.test(path))
			return result;

	path = path.split(/\//);

	while( path.length ){
		let key = path.shift();

		if (!json.hasOwnProperty(key))
			break;

		if (json.constructor()[key] !== undefined)
			break;

		if (!path.length)
			result = json[key];

		json = json[key];
	}

	return result;
}
