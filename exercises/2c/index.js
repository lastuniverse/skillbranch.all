var express = require('express');
var cors = require('cors');


const app = express();
app.use(cors());

app.get('/', function (req, res) {
  console.log(req.query);
  var result = 'Invalid username';

  if(req.query.username){
//    const re = new RegExp('^(?:https?\\:)?\\@*(?:(?://)?\\w+\\.\\w+(?:\\.\\w+)*/)?(.*?)(?:(?:\\?|/).*)?$','');
    const re = new RegExp('^(?:https?\\:)?\\@*(?:(?://)?.*?/)?\\@*(.*?)(?:(?:\\?|/).*)?$','');
//    const re /^(?:https?\:)?\@*(?:(?:\/\/)?.*?\/)?\@*(.*?)(?:(?:\?|\/).*)?$/
    const username = re.exec(req.query.username);
    if( username[1] )
      result = '@'+username[1];
    console.log(username);

  }
  res.send(result);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

const data =[
  'subdomain.domain.com.uk/igorsuvorov',
  'subdomain.domain.com.uk/igor2_suvorov',
  'subdomain.domain.com.uk/igor3-suvorov',
  'subdomain.domain.com.uk/igor4suvorov',
  'subdomain.domain.com.uk/igor5.suvorov',

  'subdomain.domain.com/igorsuvorov',
  'subdomain.domain.com/igor2_suvorov',
  'subdomain.domain.com/igor3-suvorov',
  'subdomain.domain.com/igor4suvorov',
  'subdomain.domain.com/igor5.suvorov',

  'https://subdomain.domain.com.uk/igorsuvorov',
  'https://subdomain.domain.com.uk/igor2_suvorov',
  'https://subdomain.domain.com.uk/igor3-suvorov',
  'https://subdomain.domain.com.uk/igor4suvorov',
  'https://subdomain.domain.com.uk/igor5.suvorov',

  'http://subdomain.domain.com.uk/igorsuvorov',
  'http://subdomain.domain.com.uk/igor2_suvorov',
  'http://subdomain.domain.com.uk/igor3-suvorov',
  'http://subdomain.domain.com.uk/igor4suvorov',
  'http://subdomain.domain.com.uk/igor5.suvorov',

  '//subdomain.domain.com.uk/igorsuvorov',
  '//subdomain.domain.com.uk/igor2_suvorov',
  '//subdomain.domain.com.uk/igor3-suvorov',
  '//subdomain.domain.com.uk/igor4suvorov',
  '//subdomain.domain.com.uk/igor5.suvorov',

  'domain.com.uk/igorsuvorov',
  'domain.com.uk/igor2_suvorov',
  'domain.com.uk/igor3-suvorov',
  'domain.com.uk/igor4suvorov',
  'domain.com.uk/igor5.suvorov',

  'https://domain.com.uk/igorsuvorov',
  'https://domain.com.uk/igor2_suvorov',
  'https://domain.com.uk/igor3-suvorov',
  'https://domain.com.uk/igor4suvorov',
  'http://domain.com.uk/igorsuvorov',
  'https://domain.com.uk/igor5.suvorov',

  'http://domain.com.uk/igor2_suvorov',
  'http://domain.com.uk/igor3-suvorov',
  'http://domain.com.uk/igor4suvorov',
  'http://domain.com.uk/igor5.suvorov',

  'domain.com/igorsuvorov',
  'domain.com/igor2_suvorov',
  'domain.com/igor3-suvorov',
  'domain.com/igor4suvorov',
  'domain.com/igor5.suvorov',

  '//domain.com/igorsuvorov',
  '//domain.com/igor2_suvorov',
  '//domain.com/igor3-suvorov',
  '//domain.com/igor4suvorov',
  '//domain.com/igor5.suvorov',

  'https://domain.com/igorsuvorov',
  'https://domain.com/igor2_suvorov',
  'https://domain.com/igor3-suvorov',
  'https://domain.com/igor4suvorov',
  'https://domain.com/igor5.suvorov',

  'http://domain.com/igorsuvorov',
  'http://domain.com/igor2_suvorov',
  'http://domain.com/igor3-suvorov',
  'http://domain.com/igor4suvorov',
  'http://domain.com/igor5.suvorov',

  '@skillbranch',
  '@skill93branch',
  '@skill.branch',
  '@skill_branch',
  '@skill-branch',

  'skillbranch',
  'skillbranch93',
  'skill.branch',
  'skill_branch',
  'skill-branch',

  'httpdomain.com/igorsuvorov',
  'httpdomain.com/igor2_suvorov',
  'httpdomain.com/igor3-suvorov',
  'httpdomain.com/igor4suvorov',
  'httpdomain.com/igor5.suvorov',
];

for( var key in data ){
  var result = 'Invalid username';
  const re = /^(?:https?\:)?\@*(?:(?:\/\/)?.*?\/)?\@*(.*?)(?:(?:\?|\/).*)?$/
  const username = re.exec(data[key]);
  if( username[1] )
    result = '@'+username[1];
  console.log(result);
  //console.log(data[key]);
}
