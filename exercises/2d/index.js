var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var w3color = require('./w3color');

var app = express();
app.use(cors());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded


app.get('/', function(req, res) {
    var result = 'Invalid color';
    console.log("req.query: ",req.query);
    if('color' in req.query )
        result = getColor(req.query.color);
    res.send(result);
});

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});


function getColor(color) {
    let result = 'Invalid color';

    color = color.replace(/%20/g,' ');


    const rgbRe = /^\s*\#?([a-f\d])([a-f\d])([a-f\d])\s*$/i;
    if( rgbRe.test(color) ){
        const colors = rgbRe.exec(color);
        result = '#'+colors[1]+colors[1]+colors[2]+colors[2]+colors[3]+colors[3];
        result = result.toLowerCase();
    }

    const rrggbbRe = /^\s*\#?([a-f\d]{6})$/i;
    if( rrggbbRe.test(color) ){
        const colors = rrggbbRe.exec(color);
        result = '#'+colors[1];
        result = result.toLowerCase();
    }


    const rgbfRe = /^\s*(rgb\s*\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\))\s*$/i;
    if( rgbfRe.test(color) ){
        const colors = rgbfRe.exec(color);
        const red = colors[2],
              green = colors[3],
              blue = colors[4];
        if( red < 256 && green < 256 && blue < 256 ){
            result = w3color.parseColor(colors[1]).toHexString();
            result = result.toLowerCase();
        }
    }




    // Hue is a degree on the color wheel from 0 to 360. 0 is red, 120 is green, 240 is blue.
    // Saturation is a percentage value; 0% means a shade of gray and 100% is the full color.
    // Lightness is also a percentage; 0% is black, 100% is white.
    const hslfRe = /^\s*(hsl\s*\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*\))\s*$/i;
    if( hslfRe.test(color) ){
        const colors = hslfRe.exec(color);
        const hue = colors[2],
              sat = colors[3],
              lig = colors[4];
        if( hue < 361 && sat < 101 && lig < 101 ){
            result = w3color.parseColor(colors[1]).toHexString();
            result = result.toLowerCase();
        }
    }

    return result;
}


