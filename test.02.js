let text = "By varying the weights and the threshold, we can get different models of decision making. For example, suppose we instead chose a threshold of 3. Then the perceptron would decide that you should go to the festival whenever the weather was good or when both the festival was near public transit and your boyfriend or girlfriend was willing to join you";

// Не забыли что в переменной text у нас уже лежит текст для анализа количества слов? :)

// заменим в этом тексте все - что не буквы латинского алфавита на пробелы
text = text.replace(/[^a-z]+/i,' ');

// разобъем текст на массив слов? разделителем будет пробел
const arr = text.split(/\s/);

// создадим объект JS (ассоциативный массив) для того чтобы складывать в него результаты
let result = {};

// в цикле просматриваем все слова из массива слов по очереди
for(let key in arr){
  let word = arr[key];
  // проверяем есть ли такое слово в объекте с результатами
  // если есть, добавляем единичку
  // если нет, создаем ключ word и присваеваем ему 1
  if( word in result )
     result[word] = result[word] + 1;
  else
     result[word] = 1;
}
// вот и все, задача решена
console.log(result);
